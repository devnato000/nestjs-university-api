import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Student } from './entities/student.entity';
import { Like, Repository } from 'typeorm';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';
import { AddGroupToStudentDto } from './dto/add-group-to-student.dto';
import { GroupsService } from '../groups/groups.service';
import { QueryFilterDto } from '../application/dto/query.filter.dto';

@Injectable()
export class StudentsService {
  constructor(
    @InjectRepository(Student)
    private readonly studentRepository: Repository<Student>,
    private readonly groupService: GroupsService,
  ) {}

  async getAllStudents(name: string): Promise<Student[]> {
    const options = {};

    if (name) {
      options['where'] = { name: Like(`%${name}%`) };
    }

    const students = await this.studentRepository.find({
      ...options,
    });

    if (!name) {
      return students;
    }
    return students;
  }
  async getSortStudents(queryFilter: QueryFilterDto): Promise<Student[]> {
    const options = {};
    const { sortField, sortOrder } = queryFilter;

    if (sortField) {
      options['order'] = { [sortField]: sortOrder };
    }

    const students = await this.studentRepository.find({
      ...options,
    });

    return students;
  }

  async getStudentById(id: string): Promise<Student> {
    const student = await this.studentRepository
      .createQueryBuilder('student')
      .select([
        'student.id as id',
        'student.name as name',
        'student.surname as surname',
        'student.email as email',
        'student.age as age',
      ])
      .leftJoin('student.group', 'group')
      .addSelect('group.name as "groupName"')
      .where('student.id = :id', { id })
      .getRawOne();

    if (!student) {
      throw new NotFoundException('Student not found');
    }
    return student;
  }

  async getStudentByIdWithMarks(id): Promise<Student[]> {
    const student = await this.studentRepository.find({
      where: {
        id: id,
      },
      relations: ['marks'],
    });

    if (!student) {
      throw new NotFoundException('Student not found');
    }
    return student;
  }

  async createStudent(studentData: CreateStudentDto): Promise<Student> {
    const existingStudent = await this.studentRepository.findOne({
      where: {
        email: studentData.email,
      },
    });
    if (existingStudent) {
      throw new ConflictException('Student with this email already exists');
    }

    return this.studentRepository.save(studentData);
  }

  async updateStudentById(
    id: string,
    studentUpdateData: UpdateStudentDto,
  ): Promise<void> {
    const result = await this.studentRepository.update(id, studentUpdateData);

    if (result.affected === 0) {
      throw new NotFoundException('Student not found');
    }
  }

  async deleteStudentById(id: string): Promise<void> {
    const result = await this.studentRepository.delete(id);

    if (result.affected === 0) {
      throw new NotFoundException('Student not found');
    }
  }

  async addGroupToStudent(
    id: string,
    addGroupToStudentRequest: AddGroupToStudentDto,
  ): Promise<void> {
    const student = await this.getStudentById(id);
    if (!student) {
      throw new NotFoundException('Student not found');
    }
    const currentGroup = await this.groupService.getGroupById(
      addGroupToStudentRequest.group,
    );

    await this.studentRepository.update(id, {
      group: currentGroup,
    });
  }

  async addImage(id, file: Express.Multer.File): Promise<Student> {
    const student = await this.studentRepository.findOne({ where: { id } });
    if (!student) {
      throw new NotFoundException('Student not found');
    }

    // const filename = `${student.id}`;
    // fs.writeFileSync(`uploads/${filename}`, file.buffer);
    //
    // student.imagePath = filename;
    // await this.studentRepository.save(student);

    student.imagePath = file.filename;
    await this.studentRepository.save(student);
    return student;
  }

  async getImage(id): Promise<string | null> {
    const student = await this.studentRepository.findOne({ where: { id } });

    if (!student || !student.imagePath) {
      throw new NotFoundException('Student image not found');
    }

    return student.imagePath;
  }
}
