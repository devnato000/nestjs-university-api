import {
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import * as buffer from 'buffer';

export class CreateStudentDto {
  @IsNotEmpty()
  @IsEmail()
  @IsString()
  @ApiProperty()
  email: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  name: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  surname: string;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  age: number;

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  imagePath?: string;

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  imageData?: Buffer;

  @IsOptional()
  @IsString()
  @ApiPropertyOptional()
  groupId?: number;
}
