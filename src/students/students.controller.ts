import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { Student } from './entities/student.entity';
import { StudentsService } from './students.service';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';
import { AddGroupToStudentDto } from './dto/add-group-to-student.dto';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiQuery,
  ApiResponse,
} from '@nestjs/swagger';
import { AuthGuard } from '../auth/guards/auth.guard';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { QueryFilterDto } from '../application/dto/query.filter.dto';

@UseGuards(AuthGuard)
@ApiBearerAuth('jwt')
@Controller('students')
export class StudentsController {
  constructor(private readonly studentsService: StudentsService) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @Get()
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  @ApiQuery({ name: 'Name', required: false, type: String })
  async getAllStudents(@Query('Name') name?: string): Promise<Student[]> {
    return await this.studentsService.getAllStudents(name);
  }
  @UseInterceptors(ClassSerializerInterceptor)
  @Get('sort')
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async getSortStudents(
    @Query() queryFilter?: QueryFilterDto,
  ): Promise<Student[]> {
    return await this.studentsService.getSortStudents(queryFilter);
  }

  @Get(':id')
  @ApiResponse({
    status: 200,
  })
  @ApiResponse({
    status: 404,
    description: 'Student not found.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async getStudentById(
    @Param('id', ParseIntPipe) id: string,
  ): Promise<Student> {
    return await this.studentsService.getStudentById(id);
  }

  @Get(':id/marks')
  @ApiResponse({
    status: 200,
  })
  @ApiResponse({
    status: 404,
    description: 'Student not found.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async getStudentByIdWithMarks(
    @Param('id', ParseIntPipe) id: string,
  ): Promise<Student[]> {
    return await this.studentsService.getStudentByIdWithMarks(id);
  }

  @Post()
  @ApiResponse({
    status: 201,
    description: 'Student created.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 409,
    description: 'Conflict (Student with this email already exists).',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async createStudent(
    @Body() studentCreateRequest: CreateStudentDto,
  ): Promise<Student> {
    return await this.studentsService.createStudent(studentCreateRequest);
  }

  @Patch(':id')
  @ApiResponse({
    status: 204,
    description: 'Student patched.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 409,
    description: 'Conflict (Student with this email already exists).',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async updateStudentById(
    @Param('id', ParseIntPipe) id: string,
    @Body() studentUpdateRequest: UpdateStudentDto,
  ): Promise<void> {
    return await this.studentsService.updateStudentById(
      id,
      studentUpdateRequest,
    );
  }

  @Patch(':id/add-group')
  @ApiResponse({
    status: 204,
    description: 'Group added.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 409,
    description: 'Conflict.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async addGroupToStudent(
    @Param('id', ParseIntPipe) id: string,
    @Body() addGroupToStudentRequest: AddGroupToStudentDto,
  ): Promise<void> {
    return await this.studentsService.addGroupToStudent(
      id,
      addGroupToStudentRequest,
    );
  }

  @Delete(':id')
  @ApiResponse({
    status: 204,
    description: 'Student deleted.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async deleteStudentById(
    @Param('id', ParseIntPipe) id: string,
  ): Promise<void> {
    await this.studentsService.deleteStudentById(id);
  }

  @ApiResponse({ status: 201, description: 'Image uploaded successfully' })
  @Post('upload/:id')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, cb) => {
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          return cb(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
    }),
  )
  async uploadImage(
    @Param('id') id: string,
    @UploadedFile() file: Express.Multer.File,
  ) {
    await this.studentsService.addImage(id, file);
    return { imagePath: file.path };
  }

  @Get('image/:id')
  @ApiResponse({
    status: 200,
    description: 'Image retrieved successfully',
  })
  async getImage(@Param('id') id: number, @Res() res: any) {
    try {
      const filename = await this.studentsService.getImage(id);

      res.send(filename);
    } catch (error) {
      res.status(error).json({ message: error });
    }
  }
}
