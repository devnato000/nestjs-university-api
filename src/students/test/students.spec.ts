import { Test, TestingModule } from '@nestjs/testing';
import { Students } from '../students';

describe('Students', () => {
  let provider: Students;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [Students],
    }).compile();

    provider = module.get<Students>(Students);
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });
});
