import { Module } from '@nestjs/common';
import { StudentsModule } from '../students/students.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GroupsModule } from '../groups/groups.module';
import { CoursesModule } from '../courses/courses.module';
import { LectorsModule } from '../lectors/lectors.module';
import { MarksModule } from '../marks/marks.module';
import { PostsModule } from '../posts/posts.module';
import { typeOrmAsyncConfig } from '../configs/database/typeorm-config';
import { AuthModule } from '../auth/auth.module';
import { AuthControllerModule } from '../auth/auth.controller.module';
import { ResetTokenModule } from '../reset-token/reset-token.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { MaileModule } from '../mailer/maile.module';

@Module({
  imports: [
    TypeOrmModule.forRootAsync(typeOrmAsyncConfig),
    StudentsModule,
    GroupsModule,
    CoursesModule,
    LectorsModule,
    PostsModule,
    MarksModule,
    AuthModule,
    AuthControllerModule,
    ResetTokenModule,
    MaileModule,
    MailerModule.forRootAsync({
      useFactory: () => ({
        transport: {
          host: 'mailcatcher',
          port: 1025,
          ignoreTLS: true,
        },
        defaults: {
          from: '"Nest Mailer" <your-email@example.com>',
        },
      }),
    }),
  ],
})
export class AppModule {}
