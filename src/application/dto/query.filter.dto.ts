import { IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class QueryFilterDto {
  @IsString()
  @IsOptional()
  @ApiProperty({ required: false, description: 'Sort field' })
  sortField: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ required: false, description: 'Sort order (default: DESC)' })
  sortOrder: string = 'DESC';
}
