import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Lector } from './entities/lector.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { CreateLectorDto } from './dto/create-lector.dto';
import { UpdateLectorAuthDto } from './dto/update-lector-auth.dto';
import { hashPassword } from '../utils/password-hash';
import { UpdateLectorByIdDtoDto } from './dto/update-lector--by-id.dto';

@Injectable()
export class LectorsService {
  constructor(
    @InjectRepository(Lector)
    private readonly lectorRepository: Repository<Lector>,
  ) {}

  async getAllLectors(): Promise<Lector[]> {
    return await this.lectorRepository.find();
  }
  async getLectorsById(id: string): Promise<Lector[]> {
    return await this.lectorRepository.find({
      where: {
        id: id,
      },
    });
  }

  async getLectorById(id): Promise<Lector> {
    const lector = await this.lectorRepository.findOneBy({
      id: id,
    });

    if (!lector) {
      throw new NotFoundException('Lector not found');
    }
    return lector;
  }

  async findByEmail(email: string): Promise<Lector | undefined> {
    const lector = await this.lectorRepository.findOneBy({
      email: email,
    });

    if (!lector) {
      throw new NotFoundException('Email not found');
    }
    return lector;
  }

  async getLectorWithCoursesById(id): Promise<Lector[]> {
    const lector = await this.lectorRepository.find({
      where: { id: id },
      relations: ['courses'],
    });

    if (!lector) {
      throw new NotFoundException('Lector not found');
    }
    return lector;
  }

  async createLector(lectorCreateData: CreateLectorDto): Promise<Lector> {
    lectorCreateData.password = await hashPassword(lectorCreateData.password);
    const existingLector = await this.lectorRepository.findOne({
      where: {
        email: lectorCreateData.email,
      },
    });
    if (existingLector) {
      throw new ConflictException('Lector with this email already exists');
    }
    return this.lectorRepository.save(lectorCreateData);
  }

  async updateLectorByIdAuth(
    id: string,
    lectorUpdateData: UpdateLectorAuthDto,
  ): Promise<UpdateResult> {
    lectorUpdateData.password = await hashPassword(lectorUpdateData.password);
    const result = await this.lectorRepository.update(id, lectorUpdateData);
    if (result.affected === 0) {
      throw new NotFoundException('Lector not found');
    }
    return result;
  }
  async updateLectorById(
    id: string,
    lectorUpdateData: UpdateLectorByIdDtoDto,
  ): Promise<UpdateResult> {
    const result = await this.lectorRepository.update(id, lectorUpdateData);
    if (result.affected === 0) {
      throw new NotFoundException('Lector not found');
    }
    return result;
  }

  async deleteLectorById(id: string): Promise<DeleteResult> {
    const result = await this.lectorRepository.delete(id);

    if (result.affected === 0) {
      throw new NotFoundException('Lector not found');
    }
    return result;
  }
}
