import { Injectable, NotFoundException } from '@nestjs/common';
import { LectorsService } from './lectors.service';

@Injectable()
export class LectorsControllerService {
  constructor(private readonly usersService: LectorsService) {}

  public async findCurrentUser(lectorId: string) {
    const lector = await this.usersService.getLectorById(lectorId);
    delete lector.password;
    if (!lector) {
      throw new NotFoundException(`Lector is not found`);
    }
    return lector;
  }
}
