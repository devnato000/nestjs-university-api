import { Test, TestingModule } from '@nestjs/testing';
import { LectorsController } from '../lectors.controller';

describe('LectorsController', () => {
  let controller: LectorsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LectorsController],
    }).compile();

    controller = module.get<LectorsController>(LectorsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
