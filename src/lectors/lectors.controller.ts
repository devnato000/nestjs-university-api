import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { LectorsService } from './lectors.service';
import { Lector } from './entities/lector.entity';
import { UpdateResult } from 'typeorm';
import { CreateLectorDto } from './dto/create-lector.dto';
import { UpdateLectorAuthDto } from './dto/update-lector-auth.dto';
import { ApiBearerAuth, ApiResponse } from '@nestjs/swagger';
import { AuthGuard } from '../auth/guards/auth.guard';
import { CurrentUser } from '../auth/decorators/current.user.decorator';
import { LectorsControllerService } from './lectors.controller.service';
import { UpdateLectorByIdDtoDto } from './dto/update-lector--by-id.dto';

@UseGuards(AuthGuard)
@ApiBearerAuth('jwt')
@Controller('lectors')
export class LectorsController {
  constructor(
    private readonly lectorsService: LectorsService,
    private readonly LectorsControllerService: LectorsControllerService,
  ) {}

  @Get('me')
  public async findMe(@CurrentUser() loggedLector: Lector) {
    const response = await this.LectorsControllerService.findCurrentUser(
      loggedLector.id,
    );
    return response;
  }

  @Post()
  @ApiResponse({
    status: 201,
    description: 'Lector created.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 409,
    description: 'Conflict.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async createLector(
    @Body() lectorCreateRequest: CreateLectorDto,
  ): Promise<Lector> {
    return await this.lectorsService.createLector(lectorCreateRequest);
  }

  @Get(':id/courses')
  @ApiResponse({
    status: 200,
  })
  @ApiResponse({
    status: 404,
    description: 'Lector not found.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async getLectorWithCoursesById(@Param('id') id: string): Promise<Lector[]> {
    const lector = await this.lectorsService.getLectorWithCoursesById(id);
    if (!lector) {
      throw new NotFoundException('Lector not found');
    }
    return lector;
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @Get()
  @ApiResponse({
    status: 200,
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async getAllLectors(): Promise<Lector[]> {
    return await this.lectorsService.getAllLectors();
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @Get(':id')
  @ApiResponse({
    status: 200,
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async getLectorsById(@Param('id') id: string): Promise<Lector[]> {
    return await this.lectorsService.getLectorsById(id);
  }

  @Patch(':id')
  @ApiResponse({
    status: 204,
    description: 'Lector patched.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 409,
    description: 'Conflict.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async updateLectorByIdAuth(
    @Param('id', ParseIntPipe) id: string,
    @Body() lectorUpdateRequest: UpdateLectorAuthDto,
  ): Promise<UpdateResult> {
    const lector = await this.lectorsService.updateLectorByIdAuth(
      id,
      lectorUpdateRequest,
    );
    if (!lector) {
      throw new NotFoundException('Lector not found');
    }
    return lector;
  }

  @Patch('/update/:id')
  @ApiResponse({
    status: 204,
    description: 'Lector patched.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 409,
    description: 'Conflict.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async updateLectorById(
    @Param('id', ParseIntPipe) id: string,
    @Body() lectorUpdateRequest: UpdateLectorByIdDtoDto,
  ): Promise<UpdateResult> {
    const lector = await this.lectorsService.updateLectorById(
      id,
      lectorUpdateRequest,
    );
    if (!lector) {
      throw new NotFoundException('Lector not found');
    }
    return lector;
  }

  @Delete(':id')
  @ApiResponse({
    status: 204,
    description: 'Lector deleted.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async deleteLectorById(@Param('id') id: string): Promise<void> {
    const lector = await this.lectorsService.deleteLectorById(id);
    if (!lector) {
      throw new NotFoundException('Lector not found');
    }
  }
}
