import {
  Body,
  Controller,
  Delete,
  Param,
  ParseIntPipe,
  Post,
  UseGuards,
} from '@nestjs/common';
import { MarksService } from './marks.service';
import { CreateMarkDto } from './dto/mark-create-request.dto';
import { ApiBearerAuth, ApiResponse } from '@nestjs/swagger';
import { AuthGuard } from '../auth/guards/auth.guard';

@UseGuards(AuthGuard)
@ApiBearerAuth('jwt')
@Controller('marks')
export class MarksController {
  constructor(private readonly marksService: MarksService) {}

  @Post()
  @ApiResponse({
    status: 201,
    description: 'Mark created.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async createMark(@Body() addMarkRequest: CreateMarkDto): Promise<void> {
    const { mark, course, student, lector } = addMarkRequest;
    const markData = { mark, course, student, lector };
    await this.marksService.createMark(markData);
  }

  @Delete(':id')
  @ApiResponse({
    status: 204,
    description: 'Mark deleted.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async deleteMark(@Param('id', ParseIntPipe) id: string): Promise<void> {
    await this.marksService.deleteMark(id);
  }
}
