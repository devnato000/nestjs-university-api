import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Mark } from './entities/mark.entity';
import { Repository } from 'typeorm';
import { Course } from '../courses/entities/course.entity';
import { Student } from '../students/entities/student.entity';
import { Lector } from '../lectors/entities/lector.entity';
import { CreateMarkDto } from './dto/mark-create-request.dto';

@Injectable()
export class MarksService {
  constructor(
    @InjectRepository(Mark)
    private readonly markRepository: Repository<Mark>,
    @InjectRepository(Course)
    private readonly courseRepository: Repository<Course>,
    @InjectRepository(Student)
    private readonly studentRepository: Repository<Student>,
    @InjectRepository(Lector)
    private readonly lectorRepository: Repository<Lector>,
  ) {}

  async createMark(markCreateData: CreateMarkDto): Promise<void> {
    const { mark, course, student, lector } = markCreateData;

    const isCourse = await this.courseRepository.findOneBy({
      id: course,
    });
    const isStudent = await this.studentRepository.findOneBy({
      id: student,
    });
    const isLector = await this.lectorRepository.findOneBy({
      id: lector,
    });

    if (!isCourse) {
      throw new NotFoundException('Course not found');
    }
    if (!isStudent) {
      throw new NotFoundException('Student not found');
    }
    if (!isLector) {
      throw new NotFoundException('Lector not found');
    }

    await this.markRepository.insert({
      mark: mark,
      student: isStudent,
      lector: isLector,
      course: isCourse,
    });
  }

  async deleteMark(id: string): Promise<void> {
    const result = await this.markRepository.delete(id);

    if (result.affected === 0) {
      throw new NotFoundException('Mark not found');
    }
  }
}
