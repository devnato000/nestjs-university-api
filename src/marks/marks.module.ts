import { Module } from '@nestjs/common';
import { MarksController } from './marks.controller';
import { MarksService } from './marks.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Mark } from './entities/mark.entity';
import { Student } from '../students/entities/student.entity';
import { Lector } from '../lectors/entities/lector.entity';
import { Course } from '../courses/entities/course.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Mark]),
    TypeOrmModule.forFeature([Student]),
    TypeOrmModule.forFeature([Lector]),
    TypeOrmModule.forFeature([Course]),
  ],
  controllers: [MarksController],
  providers: [MarksService],
})
export class MarksModule {}
