import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Group } from './entities/group.entity';
import { Repository } from 'typeorm';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';

@Injectable()
export class GroupsService {
  constructor(
    @InjectRepository(Group)
    private readonly groupsRepository: Repository<Group>,
  ) {}

  async getAllGroups(): Promise<Group[]> {
    return this.groupsRepository.find();
  }

  async getAllGroupsWithStudents(): Promise<Group[]> {
    return this.groupsRepository.find({ relations: ['student'] });
  }

  async getGroupById(id: string): Promise<Group> {
    return this.groupsRepository.findOne({
      where: { id },
    });
  }

  async getStudentByGroupId(id: string): Promise<Group> {
    const group = await this.groupsRepository
      .createQueryBuilder('groups')
      .leftJoinAndSelect('groups.student', 'student')
      .where('groups.id = :id', { id })
      .getOne();

    if (!group) {
      throw new NotFoundException('Group not found');
    }
    return group;
  }

  async createGroup(groupData: CreateGroupDto): Promise<Group> {
    const existingGroup = await this.groupsRepository.findOne({
      where: {
        name: groupData.name,
      },
    });
    if (existingGroup) {
      throw new ConflictException('Group with this name already exists');
    }

    return this.groupsRepository.save(groupData);
  }

  async updateGroupById(
    id: string,
    groupUpdateData: UpdateGroupDto,
  ): Promise<void> {
    const result = await this.groupsRepository.update(id, groupUpdateData);
    if (result.affected === 0) {
      throw new NotFoundException('Group not found');
    }
  }

  async deleteGroupById(id: string): Promise<void> {
    const group = await this.getStudentByGroupId(id);

    if (!group) {
      throw new NotFoundException('Group not found');
    }

    await this.groupsRepository.delete(id);
  }
}
