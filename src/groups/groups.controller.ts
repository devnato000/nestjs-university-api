import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { GroupsService } from './groups.service';
import { Group } from './entities/group.entity';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { ApiBearerAuth, ApiResponse } from '@nestjs/swagger';
import { AuthGuard } from '../auth/guards/auth.guard';

@UseGuards(AuthGuard)
@ApiBearerAuth('jwt')
@Controller('groups')
export class GroupsController {
  constructor(private readonly groupsService: GroupsService) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @Get()
  async getAllGroups(): Promise<Group[]> {
    return await this.groupsService.getAllGroups();
  }

  @Get('students')
  async getAllGroupsWithStudents(): Promise<Group[]> {
    return await this.groupsService.getAllGroupsWithStudents();
  }

  @Get(':id')
  @ApiResponse({
    status: 200,
  })
  @ApiResponse({
    status: 404,
    description: 'Group not found.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async getGroupWithStudents(@Param('id') id: string): Promise<Group> {
    return await this.groupsService.getStudentByGroupId(id);
  }

  @Post()
  @ApiResponse({
    status: 201,
    description: 'Group created.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 409,
    description: 'Conflict.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async createGroup(
    @Body() groupCreateRequest: CreateGroupDto,
  ): Promise<Group> {
    return await this.groupsService.createGroup(groupCreateRequest);
  }

  @Patch(':id')
  @ApiResponse({
    status: 204,
    description: 'Group patched.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 409,
    description: 'Conflict.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async updateGroupById(
    @Param('id') id: string,
    @Body() groupUpdateRequest: UpdateGroupDto,
  ): Promise<void> {
    const group = await this.groupsService.updateGroupById(
      id,
      groupUpdateRequest,
    );
    return group;
  }

  @Delete(':id')
  @ApiResponse({
    status: 204,
    description: 'Group deleted.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async deleteGroupById(@Param('id') id: string): Promise<void> {
    const group = await this.groupsService.deleteGroupById(id);

    if (group !== null) {
      throw new NotFoundException('Group not found');
    }
  }
}
