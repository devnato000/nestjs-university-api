import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { Course } from './entities/course.entity';
import { LectorsService } from '../lectors/lectors.service';
import { LectorCourse } from '../lectors/entities/lector_course.entity';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(Course)
    private readonly courseRepository: Repository<Course>,
    @InjectRepository(LectorCourse)
    private readonly lectorCourseRepository: Repository<LectorCourse>,
    private lectorsServices: LectorsService,
  ) {}

  async getAllCourses(): Promise<Course[]> {
    const courses = this.courseRepository.find();
    return courses;
  }

  async getCourseById(courseId): Promise<Course> {
    const course = await this.courseRepository.findOneBy({ id: courseId });

    if (!course) {
      throw new NotFoundException('Course not found');
    }

    return course;
  }

  async getCoursesWithMarks(id: string): Promise<Course[]> {
    const course = await this.courseRepository.find({
      where: {
        id: id,
      },
      relations: {
        marks: {
          student: true,
        },
      },
    });

    if (!course) {
      throw new NotFoundException('Course not found');
    }

    return course;
  }

  async getCoursesByLectorId(lectorId: string): Promise<Course[]> {
    const lectorCourses = await this.lectorCourseRepository
      .createQueryBuilder('lector')
      .innerJoinAndSelect('lector.courses', 'courses')
      .where('lector.lectorId = :lectorId', { lectorId })
      .getMany();

    const courses = lectorCourses.flatMap((lc) => lc.courses);

    return courses;
  }

  async createCourse(courseCreateSchema: CreateCourseDto): Promise<Course> {
    const course = await this.courseRepository.findOne({
      where: {
        name: courseCreateSchema.name,
      },
    });

    if (course) {
      throw new NotFoundException('Course with this name already exists');
    }

    return this.courseRepository.save(courseCreateSchema);
  }

  async updateCourseById(
    id: string,
    courseUpdateSchema: UpdateCourseDto,
  ): Promise<UpdateResult> {
    const result = await this.courseRepository.update(id, courseUpdateSchema);

    if (!result.affected) {
      throw new NotFoundException('Course with this id is not found');
    }

    return result;
  }

  async addLectorToCourse(courseId: string, lectorId: string): Promise<void> {
    const course = await this.getCourseById(courseId);
    const lector = await this.lectorsServices.getLectorById(lectorId);

    if (!course) {
      throw new NotFoundException('Course not found');
    }
    if (!lector) {
      throw new NotFoundException('Lector not found');
    }

    await this.lectorCourseRepository
      .createQueryBuilder()
      .insert()
      .into(LectorCourse)
      .values({
        lectorId: lectorId,
        courseId: courseId,
      })
      .execute();
  }

  async deleteCourseById(id: string): Promise<DeleteResult> {
    const result = await this.courseRepository.delete(id);

    if (!result.affected) {
      throw new NotFoundException('Course with this id is not found');
    }

    return result;
  }
}
