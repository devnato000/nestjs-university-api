import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  NotFoundException,
  Param,
  Patch,
  Post,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { Course } from './entities/course.entity';
import { CoursesService } from './courses.service';
import { UpdateResult } from 'typeorm';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { ApiBearerAuth, ApiResponse } from '@nestjs/swagger';
import { AuthGuard } from '../auth/guards/auth.guard';

@UseGuards(AuthGuard)
@ApiBearerAuth('jwt')
@Controller('courses')
export class CoursesController {
  constructor(private readonly coursesService: CoursesService) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @Get()
  async getAllCourses(): Promise<Course[]> {
    return await this.coursesService.getAllCourses();
  }

  @Get('lectors/:lectorId')
  @ApiResponse({
    status: 200,
  })
  @ApiResponse({
    status: 404,
    description: 'Course not found.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async getCoursesByLectorId(
    @Param('lectorId') lectorId: string,
  ): Promise<Course[]> {
    return await this.coursesService.getCoursesByLectorId(lectorId);
  }

  @Get(':id')
  @ApiResponse({
    status: 200,
  })
  @ApiResponse({
    status: 404,
    description: 'Course not found.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async getCoursesById(@Param('id') id: string): Promise<Course> {
    return await this.coursesService.getCourseById(id);
  }

  @Get(':id/marks')
  @ApiResponse({
    status: 200,
  })
  @ApiResponse({
    status: 404,
    description: 'Course not found.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async getCoursesWithMarks(@Param('id') id: string): Promise<Course[]> {
    return await this.coursesService.getCoursesWithMarks(id);
  }

  @Post()
  @ApiResponse({
    status: 201,
    description: 'Course created.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 409,
    description: 'Conflict.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async createCourse(
    @Body() courseCreateRequest: CreateCourseDto,
  ): Promise<Course> {
    return await this.coursesService.createCourse(courseCreateRequest);
  }

  @Patch(':id')
  @ApiResponse({
    status: 204,
    description: 'Course patched.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 409,
    description: 'Conflict.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async updateCourseById(
    @Param('id') id: string,
    @Body() courseUpdateRequest: UpdateCourseDto,
  ): Promise<UpdateResult> {
    const course = await this.coursesService.updateCourseById(
      id,
      courseUpdateRequest,
    );
    if (!course) {
      throw new NotFoundException('Course not found');
    }
    return course;
  }

  @Patch(':courseId/add-lector/:lectorId')
  @ApiResponse({
    status: 204,
    description: 'Course patched.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 409,
    description: 'Conflict.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async addLectorToCourse(
    @Param('courseId') courseId: string,
    @Param('lectorId') lectorId: string,
  ): Promise<void> {
    await this.coursesService.addLectorToCourse(courseId, lectorId);
  }

  @Delete(':id')
  @ApiResponse({
    status: 204,
    description: 'Course deleted.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async deleteCourseById(@Param('id') id: string): Promise<void> {
    const course = await this.coursesService.deleteCourseById(id);
    if (!course) {
      throw new NotFoundException('Course not found');
    }
  }
}
