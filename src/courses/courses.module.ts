import { Module } from '@nestjs/common';
import { CoursesController } from './courses.controller';
import { CoursesService } from './courses.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Course } from './entities/course.entity';
import { Lector } from '../lectors/entities/lector.entity';
import { LectorsService } from '../lectors/lectors.service';
import { LectorCourse } from '../lectors/entities/lector_course.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Course]),
    TypeOrmModule.forFeature([Lector]),
    TypeOrmModule.forFeature([LectorCourse]),
  ],
  controllers: [CoursesController],
  providers: [CoursesService, LectorsService],
})
export class CoursesModule {}
