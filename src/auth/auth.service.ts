import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ResetTokenService } from '../reset-token/reset-token.service';
import { ResetTokenInterface } from '../reset-token/interfaces/reset-token.interface';
import { SignResponseDto } from './dto/sign.response.dto';
import { ResetPasswordWithTokenRequestDto } from './dto/reset-password-with-token.request.dto';
import { LectorsService } from '../lectors/lectors.service';
import { isPasswordCorrect } from '../utils/password-hash';
import { MaileService } from '../mailer/maile.service';

@Injectable()
export class AuthService {
  constructor(
    private lectorsService: LectorsService,
    private jwtService: JwtService,
    private resetTokenService: ResetTokenService,
    private mailerService: MaileService,
  ) {}

  public async signIn(email: string, pass: string): Promise<SignResponseDto> {
    const lector = await this.lectorsService.findByEmail(email);

    if (!(await isPasswordCorrect(pass, lector.password))) {
      throw new UnauthorizedException('Password incorrect');
    }

    const payload = {
      id: lector.id,
      email: lector.email,
      name: lector.name,
    };

    return {
      accessToken: await this.jwtService.signAsync(payload),
    };
  }

  public async signUp(email: string, pass: string): Promise<SignResponseDto> {
    await this.lectorsService.createLector({
      email: email,
      password: pass,
    });
    const payload = { sub: email, password: pass };
    return {
      accessToken: await this.jwtService.signAsync(payload),
    };
  }

  public async resetPasswordRequest(
    email: string,
  ): Promise<ResetTokenInterface> {
    const lector = await this.lectorsService.findByEmail(email);
    if (!lector) {
      throw new BadRequestException(
        `Cannot generate token for reset password request  because user with email:${email} is not found`,
      );
    }
    const token = await this.resetTokenService.generateResetToken(email);

    await this.mailerService.sendEmail(email, 'password reset', token.token);

    return;
  }

  public async resetPassword(
    token: string,
    resetPasswordWithTokenRequestDto: ResetPasswordWithTokenRequestDto,
  ): Promise<void> {
    const { newPassword } = resetPasswordWithTokenRequestDto;
    const resetPasswordRequest = await this.resetTokenService.getResetToken(
      token,
    );
    if (!resetPasswordRequest) {
      throw new BadRequestException(`There is no request password request`);
    }
    const lector = await this.lectorsService.findByEmail(
      resetPasswordRequest.email,
    );
    if (!lector) {
      throw new BadRequestException(`lector is not found`);
    }

    await this.lectorsService.updateLectorByIdAuth(lector.id, {
      password: newPassword,
    });
    await this.resetTokenService.removeResetToken(token);
  }
}
