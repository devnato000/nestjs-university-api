import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Put,
  Delete,
  ParseUUIDPipe,
} from '@nestjs/common';
import { PostsService } from './posts.service';
import { Post as PostEntity } from './entities/post.entity';
import { PostCreateRequestDto } from './dto/post-create-request.dto';
import { PostUpdateRequestDto } from './dto/post-update-request.dto';
import { ApiResponse } from '@nestjs/swagger';

@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  @Get()
  async getAllPosts(): Promise<PostEntity[]> {
    return this.postsService.getAllPosts();
  }

  @Get(':id')
  @ApiResponse({
    status: 200,
  })
  @ApiResponse({
    status: 404,
    description: 'post not found.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async getPostById(
    @Param('id', ParseUUIDPipe) id: string,
  ): Promise<PostEntity> {
    return this.postsService.getPostById(id);
  }

  @Post()
  @ApiResponse({
    status: 201,
    description: 'Post created.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 409,
    description: 'Conflict.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async createPost(
    @Body() postCreateRequestDto: PostCreateRequestDto,
  ): Promise<PostEntity> {
    return this.postsService.createPost(postCreateRequestDto);
  }

  @Put(':id')
  @ApiResponse({
    status: 204,
    description: 'Post patched.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async updatePost(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() postUpdateRequestDto: PostUpdateRequestDto,
  ): Promise<PostEntity> {
    return this.postsService.updatePost(id, postUpdateRequestDto);
  }

  @Delete(':id')
  @ApiResponse({
    status: 204,
    description: 'Post deleted.',
  })
  @ApiResponse({
    status: 400,
    description: 'Bad Request.',
  })
  @ApiResponse({
    status: 505,
    description:
      'The HTTP version used in the request is not supported by the server.',
  })
  async deletePost(@Param('id', ParseUUIDPipe) id: string): Promise<void> {
    return this.postsService.deletePost(id);
  }
}
