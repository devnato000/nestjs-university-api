import { PartialType } from '@nestjs/mapped-types';
import { CreateStudentDto } from '../../students/dto/create-student.dto';

export class PostUpdateRequestDto extends PartialType(CreateStudentDto) {}
