import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Post } from './entities/post.entity';
import { PostCreateRequestDto } from './dto/post-create-request.dto';
import { PostUpdateRequestDto } from './dto/post-update-request.dto';

@Injectable()
export class PostsService {
  constructor(
    @InjectRepository(Post)
    private readonly postsRepository: Repository<Post>,
  ) {}

  async getAllPosts(): Promise<Post[]> {
    return this.postsRepository.find({
      order: {
        createdAt: 'DESC',
      },
    });
  }

  async getPostById(id: string): Promise<Post | null> {
    const post = await this.postsRepository.findOneBy({ id });
    if (!post) {
      throw new NotFoundException('Post not found');
    }
    return post;
  }

  async createPost(postCreateDto: PostCreateRequestDto): Promise<Post> {
    const post = this.postsRepository.create(postCreateDto);
    return this.postsRepository.save(post);
  }

  async updatePost(
    id: string,
    postUpdateDto: PostUpdateRequestDto,
  ): Promise<Post> {
    const post = await this.getPostById(id);
    Object.assign(post, postUpdateDto);
    return this.postsRepository.save(post);
  }

  async deletePost(id: string): Promise<void> {
    const post = await this.getPostById(id);
    await this.postsRepository.remove(post);
  }
}
