import { Module } from '@nestjs/common';
import { MaileService } from './maile.service';
import { EmailController } from './maile.controller';

@Module({
  controllers: [EmailController],
  providers: [MaileService],
  exports: [MaileService],
})
export class MaileModule {}
