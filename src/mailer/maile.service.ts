import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class MaileService {
  constructor(private readonly mailerService: MailerService) {}

  async sendEmail(to: string, subject: string, token: string): Promise<void> {
    await this.mailerService.sendMail({
      from: 'university@edu.com',
      to,
      subject,
      html:
        '<p>Click <a href="http://localhost:3010/reset-password/' +
        token +
        '">here</a> to reset your password</p>',
    });
  }
}
