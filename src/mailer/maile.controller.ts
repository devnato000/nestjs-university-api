import { Controller, Post, Body, UseGuards } from '@nestjs/common';
import { CreateMailDto } from './dto/create-mail.dto';
import { MaileService } from './maile.service';
import { AuthGuard } from '../auth/guards/auth.guard';
import { ApiBearerAuth } from '@nestjs/swagger';

@UseGuards(AuthGuard)
@ApiBearerAuth('jwt')
@Controller('email')
export class EmailController {
  constructor(private readonly mailerService: MaileService) {}

  @Post('/send')
  async sendEmail(@Body() emailDto: CreateMailDto) {
    const { to, subject, token } = emailDto;
    await this.mailerService.sendEmail(to, subject, token);
    return 'Email sent successfully';
  }
}
